# PostgreSQL Crash Course

Dont waste your time on YouTube or searching around if you don't know PostgreSQL. Just refer to the documentation on W3Schools for more information.

[W3Schools PostgreSQL](https://www.w3schools.com/postgresql/index.php)

Use this project just for the projects listed below

- [Flags Quiz](./Flags%20Quiz/)
- [World Capital Quiz](./World%20Capital%20Quiz/)
